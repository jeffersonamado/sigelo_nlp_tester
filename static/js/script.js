$(document).ready(function() {
    const URL = "http://eiscapp.univalle.edu.co/ingi/nlpapi/v1.0/answer/"
//    const URL = "http://localhost:9000/api/v1.0/answer/"
    const addAnswer=(message)=>{
        $( ".messages" ).append(
            '<li class="message left appeared">'+
                '<div class="avatar">'+message.botname+'</div>'+
                '<div class="text_wrapper">'+
                    '<div class="text">'+message.text+'<br/>'+
                        '<span class="time_date">'+new Date().toLocaleTimeString()+'</span>'+
                    '</div>'+
                '</div>'+
            '</li>'
        );
        console.log( $(".messages").outerHeight(true) );
        $( ".messages" ).animate({scrollTop: $(".messages").outerHeight(true)*10 }, 800);
    }
    const addQuestion=(message)=>{
        $( ".messages" ).append(
            '<li class="message right appeared">'+
                '<div class="avatar">Tú</div>'+
                '<div class="text_wrapper">'+
                    '<div class="text">'+message+'<br/>'+
                        '<span class="time_date">'+new Date().toLocaleTimeString()+'</span>'+
                    '</div>'+
                '</div>'+
            '</li>'
        );
        $( ".messages" ).animate({scrollTop: $(".messages").outerHeight(true)*10 }, 800);
    }
    const sendMessage=()=>{
        addQuestion($("#question").val())
        var payload = {
            "complete": null,
            "message": $("#question").val(),
            "intent": {},
            "parameters": [],
            "context":{},
            "algorithm":{},
            "currentNode": "",
            "isSpeechResponse": false
        };
        $("#question").val("")
        $("#question").prop('disabled', true);
        $.ajax({
          url : URL,
          type: 'POST',
          dataType : "json",
          contentType: 'application/json',
          beforeSend: function(xhr){
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('token', 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InJlbW90ZV9hZGRyIjoiMTI3LjAuMC4xIiwiYm90bmFtZSI6IlJhZmFlbCIsInN1YiI6Ijc0YmUxNjk3OTcxMGQ0YzRlN2M2NjQ3ODU2MDg4NDU2IiwiaWF0IjoxNTkxMTg1MjI3LjUwNzc3MiwiZXhwIjoxNTkxMTg1MjI3LjUwNzc3Mn19.RTrpMkXNw3B5kKawziiHmxHdoLFGiE_5cUGKAuPIg0mhpGgB1Bs9V9MkAzqL82xlfEXogycSgXNjSLrjOu5Kgw');
          },
          success: function (result) {
            message = result.response
            message.botname = result.botname
            if(result.response.confidence == 0){
                message.text = "Lo siento. No entiendo la pregunta."
            }
            addAnswer(message)
            $("#question").prop('disabled', false);
            $("#question").val("")
            $("#question").focus()
          },
          data: JSON.stringify(payload)

        });
    }
    $.ajax({
        url: URL,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
          beforeSend: function(xhr){
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('token', 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InJlbW90ZV9hZGRyIjoiMTI3LjAuMC4xIiwiYm90bmFtZSI6IlJhZmFlbCIsInN1YiI6Ijc0YmUxNjk3OTcxMGQ0YzRlN2M2NjQ3ODU2MDg4NDU2IiwiaWF0IjoxNTkxMTg1MjI3LjUwNzc3MiwiZXhwIjoxNTkxMTg1MjI3LjUwNzc3Mn19.RTrpMkXNw3B5kKawziiHmxHdoLFGiE_5cUGKAuPIg0mhpGgB1Bs9V9MkAzqL82xlfEXogycSgXNjSLrjOu5Kgw');
          },
        success: function (result) {
            // process result
            message = result.response
            message.botname = result.botname
            addAnswer(message)
            $("#question").prop('disabled', false);
            $("#question").val("")
            $("#question").focus()
        },
        error: function (e) {
             // log error in browser
            console.log(e);
            $("#question").val("")
        }
    });
    $("#question").on( "keydown", function(event) {
      if(event.which == 13 && $("#question").val() != ""){
        sendMessage()
      }
    });

    $("#query").click(function(e) {
        e.preventDefault();
        sendMessage()
    });
});