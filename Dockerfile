# Using official python runtime base image
FROM python:3.8
# Set the application directory
WORKDIR /tester
# Install our requirements.txt
ADD requirements.txt /tester/requirements.txt
RUN pip install -r /tester/requirements.txt

# Copy our code from the current folder to /app inside the container
COPY ./ /tester

# Make port 9000 available for links and/or publish
#EXPOSE 9001
#ENTRYPOINT ["python"]
# Define our command to be run when launching the container
#CMD ["gunicorn", "nlp_core.app:app", "-b", "127.0.0.1:9000", "--log-file", "-", "--access-logfile", "-", "--workers", "4", "--keep-alive", "0"]
ENTRYPOINT ["gunicorn", "-b", "0.0.0.0:9001", "app:app"]
# "gunicorn -b :9000 --log-level=DEBUG -w 4 nlp_core.app:app"
#"--access-logfile /home/adminstrador/japeto/gunicorn-access.log --error-logfile /home/adminstrador/japeto/gunicorn-error.log"
# gunicorn nlp_core.app:app -w 1 -b 0:9000 -t 300 --log-level debug --threads 5 --access-logfile - --error-logfile -

# gunicorn -b 0.0.0.0:9000 --access-logfile - --error-logfile - --log-level debug nlp_core.app:app
# gunicorn --workers 2 --bind 127.0.0.1:5000 --error-logfile /flask/error.log --access-logfile /flask/access.log --capture-output --log-level debug